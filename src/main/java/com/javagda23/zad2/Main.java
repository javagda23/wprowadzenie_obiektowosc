package com.javagda23.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Komputer[] komputers = new Komputer[4];
//        komputers[0] = new Komputer(200, "Apple", TypProcesora.JEDNORDZENIOWY);
//        komputers[1] = new Komputer(300, "Asus", TypProcesora.WIELORDZENIOWY);
//        komputers[2] = new Komputer(500, "Dell", TypProcesora.JEDNORDZENIOWY);
//        komputers[3] = new Komputer(700, "Lenovo", TypProcesora.WIELORDZENIOWY);
//
//        // todo: wypisanie na ekran
//        for (Komputer komputer : komputers) {
//            System.out.println(komputer);
//        }

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj moc:");
        String mocString = scanner.nextLine();
        int moc = Integer.parseInt(mocString);

        System.out.println("Podaj markę:");
        String marka = scanner.nextLine();

        // valueOf - zamieni STRING -> Typ enuma
        // warunkiem jest że nazwa enuma została co do znaku poprawnie wpisana
        // WIELORDZENIOWY
        // ' wielordzenioWY'
        //

//        if(typString.startsWith("JED")){
//            TypProcesora typ = TypProcesora.JEDNORDZENIOWY;
//        }else if()...
        TypProcesora typ = null;

        do {
            try {
                System.out.println("Podaj typ procesora:");
                String typString = scanner.nextLine();
                typ = TypProcesora.valueOf(typString.trim().toUpperCase());

            } catch (IllegalArgumentException iae) {
                System.err.println("Nie wpisałeś/łaś poprawnego typu procesora, spróbuj ponownie:");
            }
        } while (typ == null);


        Komputer komputer = new Komputer(moc, marka, typ);

        System.out.println(komputer);
    }
}
