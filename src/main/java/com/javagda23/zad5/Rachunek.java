package com.javagda23.zad5;

public class Rachunek {
    private Produkt[] produkts;

    public Rachunek(Produkt[] produkts) {
        this.produkts = produkts;
    }

    public void wypiszRachunek() {
        for (Produkt produkt : produkts) {
            System.out.println(produkt);
        }
    }

    //todo: podsumuj netto (suma kwot netto produktów na rachunku)
    public double podajSumeNetto() {
        double suma = 0.0;
        for (Produkt produkt : produkts) {
            suma += produkt.getCenaProduktu();
        }
        return suma;
    }

    //todo: podsumuj brutto (suma kwot brutto produktów na rachunku)
    public double podajSumeBrutto() {
        double suma = 0.0;
        for (Produkt produkt : produkts) {
            suma += produkt.podajCeneBrutto();
        }
        return suma;
    }

    //todo: zwróć wartość podatku (brutto - netto)
    public double podajWartoscPodatku() {
        return podajSumeBrutto() - podajSumeNetto();
    }
}
