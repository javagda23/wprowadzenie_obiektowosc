package com.javagda23.zad5;

public class Produkt {
    private String nazwa;
    private Double cenaProduktu; // < -- cena netto
    private PodatekProduktu podatekProduktu;

    public Produkt(String nazwa, Double cenaProduktu, PodatekProduktu podatekProduktu) {
        this.nazwa = nazwa;
        this.cenaProduktu = cenaProduktu;
        this.podatekProduktu = podatekProduktu;
    }

    public double podajCeneBrutto() {
        // jeśli podatek 23
        // 23/100 = 0.23 * cena netto = wartość podatku
        // cenaProduktu _+ wartość podatku = cena brutto;

        //   (dla podatku 23 metoda wyliczy)=>     100 % + 23% ceny netto
        return cenaProduktu + (cenaProduktu * (podatekProduktu.getWysokoscPodatku() / 100.0));
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Double getCenaProduktu() {
        return cenaProduktu;
    }

    public void setCenaProduktu(Double cenaProduktu) {
        this.cenaProduktu = cenaProduktu;
    }

    public PodatekProduktu getPodatekProduktu() {
        return podatekProduktu;
    }

    public void setPodatekProduktu(PodatekProduktu podatekProduktu) {
        this.podatekProduktu = podatekProduktu;
    }
}
