package com.javagda23.zad5;

public enum PodatekProduktu {
    VAT_8(8),
    VAT_23(23),
    VAT_5(5),
    NO_VAT(0);

    private int wysokoscPodatku;

    PodatekProduktu(int wysokoscPodatku) {
        this.wysokoscPodatku = wysokoscPodatku;
    }

    public int getWysokoscPodatku() {
        return wysokoscPodatku;
    }
}
