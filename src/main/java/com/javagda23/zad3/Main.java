package com.javagda23.zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj imie:");
        String imie = scanner.nextLine();

        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.nextLine();

        System.out.println("Podaj pesel:");
        String pesel = scanner.nextLine();

        //################### Załadowanie wartosci do enuma (string -> enum)
        Plec plec = null;
        do {
            System.out.println("Podaj plec:");
            String plecString = scanner.nextLine();

            try {
                plec = Plec.valueOf(plecString);
            } catch (IllegalArgumentException iae) {
                System.err.println("Błąd, nie ma takiej opcji.");
            }
        } while (plec == null);
        //###################

        Obywatel obywatel = new Obywatel(imie, nazwisko, pesel, plec);
        System.out.println(obywatel);
    }
}
