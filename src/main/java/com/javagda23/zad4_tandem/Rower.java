package com.javagda23.zad4_tandem;

public class Rower {
    private int iloscPrzerzutek;

    //    private enum typRoweru;   <- źle !!!
//    private String typRoweru; <- źle !!!
    private TypRoweru typRoweru;
    private String nazwaRoweru;

    public Rower(int iloscPrzerzutek, TypRoweru typRoweru, String nazwaRoweru) {
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.typRoweru = typRoweru;
        this.nazwaRoweru = nazwaRoweru;
    }

    public void wypiszInformacjeORowerze() {
        System.out.println("Rower " + nazwaRoweru + " ma " + iloscPrzerzutek
                + " przerzutek i " + typRoweru.getIloscMiejsc() + " miejsc.");
    }

    // todo: stwórz w mainie kilka rowerów, dodaj je do tablicy
    //  następnie przeiteruj tablicę i wypisz rowery na ekran.
}
