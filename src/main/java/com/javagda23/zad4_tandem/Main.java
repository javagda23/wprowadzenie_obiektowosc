package com.javagda23.zad4_tandem;

public class Main {
    public static void main(String[] args) {

        Rower[] rowers = new Rower[3];
        rowers[0] = new Rower(5, TypRoweru.ROWER, "składak");
        rowers[1] = new Rower(13, TypRoweru.TANDEM, "złomek");
        rowers[2] = new Rower(4, TypRoweru.TANDEM, "marian");

        // wypisanie na ekran informacji o rowerze.
        for (Rower rower : rowers) {
            rower.wypiszInformacjeORowerze();
        }
    }
}
