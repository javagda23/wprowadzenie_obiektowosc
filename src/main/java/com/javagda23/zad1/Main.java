package com.javagda23.zad1;

public class Main {
    public static void main(String[] args) {
        // metoda toString określa w jaki sposób obiekt będzie
        // konkatenowany do string'a.
        // konkatenacja - złączanie ciągów znaków (stringów)
//        System.out.println(student);

        Student[] tablicaStudentow = new Student[10];
        Student student = new Student("1", "a", "b", false, new Double[] {2.1, 3.5, 4.0, 5.3});

        tablicaStudentow[0] = student;
        tablicaStudentow[1] = new Student("2", "c", "d", true, new Double[] {1.0, 2.5, 2.0, 2.3});
        tablicaStudentow[2] = new Student("4", "e", "f", false, new Double[] {5.1, 4.6, 4.1, 4.3});
        tablicaStudentow[3] = new Student("5", "h", "g", true, new Double[] {1.0, 2.3, 1.5, 1.0});

        for (Student st : tablicaStudentow) {
            if (st != null) {
                // weryfikujemy czy obiekt nie jest równy null
                System.out.println(st + " " + st.obliczSrednia() + " - " + st.czyZdal());
//                System.out.println(st.toString()); // to jest to samo co linia wyżej (bez toString())
            }
        }

        Student odnalezionyStudent = znajdzWTablicyStudentaOIndeksie(tablicaStudentow, "40");
        System.out.println();
        System.out.println(odnalezionyStudent);
    }

    private static Student znajdzWTablicyStudentaOIndeksie(Student[] tablica, String indeks) {
        for (Student st : tablica) {
            // if będzie prawdą jeśli obie strony są prawdą
            // false
            if (st != null && st.getIndeks().equals(indeks)) { // Ania == ania
                return st;
            }
        }

        System.err.println("Nie udało się odnaleźć studenta.");
        return null;
    }
}
