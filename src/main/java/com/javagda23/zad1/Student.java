package com.javagda23.zad1;

public class Student {
    private String indeks;
    private String imie;
    private String nazwisko;
    private boolean czyMezczyzna;
    private Double[] ocenyWIndeksie;

    public Student(String indeks, String imie, String nazwisko, boolean czyMezczyzna) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.czyMezczyzna = czyMezczyzna;
    }

    public Student(String indeks, String imie, String nazwisko, boolean czyMezczyzna, Double[] ocenyWIndeksie) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.czyMezczyzna = czyMezczyzna;
        this.ocenyWIndeksie = ocenyWIndeksie;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public boolean isCzyMezczyzna() {
        return czyMezczyzna;
    }

    public void setCzyMezczyzna(boolean czyMezczyzna) {
        this.czyMezczyzna = czyMezczyzna;
    }

    public double obliczSrednia() {
        // new double[] {1.0, 2.0, null, 3.0}
        // suma = 6.0 / 4
        // suma = 6.0 / 3 (dzięki iteratorowi)
        // druga zmienna pomocnicza ( iterator ) jest aby zliczyć ilość zsumowanych ocen
        double zmiennaPomocniczaSuma = 0.0;
        int zmiennaPomocniczaIterator = 0;

        for (Double ocena : ocenyWIndeksie) {
            // na razie takich ocen nie ma, ale mogę usunąć ocenę (wstawić null) i nie brać jej pod uwagę
            // dzięki temu sprawdzeniu
            if (ocena != null) {
                zmiennaPomocniczaSuma += ocena;
                zmiennaPomocniczaIterator++;
            }
        }

        return zmiennaPomocniczaSuma / zmiennaPomocniczaIterator;
    }

    public boolean czyZdal(){
        // true - jeśli żadna z ocen nie jest <= 2.0
        for (int i = 0; i < ocenyWIndeksie.length; i++) {
            if(ocenyWIndeksie[i] != null && ocenyWIndeksie[i] <= 2.0){
                return false;
            }
        }
        return true; //
    }

    @Override
    public String toString() {
        return "Student{" +
                "indeks='" + indeks + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", czyMezczyzna=" + czyMezczyzna +
                '}';
    }
}
