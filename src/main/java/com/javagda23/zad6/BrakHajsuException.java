package com.javagda23.zad6;

public class BrakHajsuException extends RuntimeException{
    public BrakHajsuException(String opis) {
        super(opis);
    }
}
