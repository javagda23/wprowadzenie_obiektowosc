package com.javagda23.zad6;

public class NiepoprawnaKwotaException extends Exception {

    public NiepoprawnaKwotaException(String s) {
        super(s);
    }
}
