package com.javagda23.zad7;

public class CholeroException extends RuntimeException {
    public CholeroException(String s) {
        super(s);
    }
}
