package com.javagda23.zad7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę:");
        int a = scanner.nextInt();

        System.out.println("Podaj liczbę:");
        int b = scanner.nextInt();

//        try {
        if (b == 0) {
            throw new CholeroException("Pamiętaj cholero, by nie dzielić przez 0!");
        }
//        } catch (CholeroException e) {
//        getMessage wypisze komunikat podany w konstruktorze wyjątku
//            System.out.println(e.getMessage());
//            System.out.println("Zamykam program");
//            return;
//        }

        System.out.println(a / b);
    }
}
