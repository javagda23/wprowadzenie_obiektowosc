package com.javagda23.zad8;

public class Main {
    public static void main(String[] args) {
        Club club = new Club();

        Person marian = new Person("Marian", "Mariański", 20);
        Person konrad = new Person("Konrad", "Mariański", 17);

        club.enter(marian);

        try {
            club.enter(konrad);
        }catch (NoAdultException nae){
            System.out.println(nae.getMessage());
        }

        System.out.println("Koniec programu.");
    }
}
