package com.javagda23.zad8;

public class Club {

    public void enter(Person person) {
        if (person.getAge() < 18) {
            throw new NoAdultException("Nie jesteś dorosły " + person.getName() + "!");
        }
        System.out.println("Witaj w klubie " + person.getName());
    }
}
