package com.javagda23.zad8;

public class NoAdultException extends RuntimeException {
    public NoAdultException(String s) {
        super(s);
    }
}
