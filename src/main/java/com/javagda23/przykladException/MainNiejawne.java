package com.javagda23.przykladException;

import java.io.IOException;

public class MainNiejawne {
    public static void main(String[] args) {

        metoda1();
    }

    private static void metoda1() {
        System.out.println("Wejście, metoda 1");
        try {
            metoda2();
        } catch (ClassCastException re) {
            System.out.println("Złapałem");
        } catch (ArithmeticException ae) {
            System.out.println();
        }
        System.out.println("Wyjście, metoda 1");
    }

    private static void metoda2() {
        System.out.println("Wejście, metoda 2");
        metoda3();
        System.out.println("Wyjście, metoda 2");
    }

    private static void metoda3() {
        System.out.println("Komunikat");

        throw new RuntimeException("Wiadomość zią.");
    }

}
