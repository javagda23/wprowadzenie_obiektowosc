package com.javagda23.przykladException;

public class MainPodchwytliwy {
    public static void main(String[] args) {

        int a = 5;
        int b = 1;

        System.out.println(dziel(a, b));
    }

    private static double dziel(int a, int b) {
        int wynik = 0;
        try {
            wynik = a / b;
            return wynik;
        } catch (ArithmeticException ae) {
            System.out.println("Był błąd");
        }
        System.out.println("Komunikat");

        return 0.0;
    }
}
