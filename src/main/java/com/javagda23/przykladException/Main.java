package com.javagda23.przykladException;

public class Main {
    public static void main(String[] args) {

        try {
            metoda1();
        } catch (Throwable throwable) {
//            throwable.printStackTrace();
            System.out.println("yeeey, mam wiadomość");
        }
    }

    private static void metoda1() throws Throwable {
        System.out.println("Wejście, metoda 1");
        metoda2();
        System.out.println("Wyjście, metoda 1");
    }

    private static void metoda2() throws Throwable {
        System.out.println("Wejście, metoda 2");
        metoda3();
        System.out.println("Wyjście, metoda 2");
    }

    private static void metoda3() throws Throwable {
        System.out.println("Komunikat");

        throw new Throwable("Wiadomość zią.");
    }

}
