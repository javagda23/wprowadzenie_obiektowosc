package com.javagda23.przykladEnum;

public enum Kształt {
    // zostały stworzone dokładnie 3 obiekty
    TROJKAT(3), // 3
    CZWOROKAT(4), // 4
    PIECIOKAT(5); // 5

    private int iloscWierzcholkow;

    Kształt(int iloscWierzcholkow) {
        this.iloscWierzcholkow = iloscWierzcholkow;
    }

    public int getIloscWierzcholkow() {
        return iloscWierzcholkow;
    }
}
